const fondos = document.querySelectorAll(".x-horizon");
const VELOCIDAD_FONDO = 10;
const GAME_TIMER = 500;
const OBJECTS_TIMER = Math.random() * (5000 - 3000) + 3000;

const objectsArr = []

//onload check si hay max puntuacion
window.onload = () => {
    if (localStorage.getItem("maxPuntuation") != undefined)
        document.querySelector(".maxScore").innerText =
            padLeft(localStorage.getItem("maxPuntuation"), 10);
};

const keyDowns = new Stream(next => {
    document.addEventListener("keydown", next);
});
const SPACE = () => {
    document.querySelector(".pj").animate(
        [{
                top: 81 + "%"
            },
            {
                top: 60 + "%"
            },
            {
                top: 53 + "%"
            },
            {
                top: 60 + "%"
            },
            {
                top: 81 + "%"
            }
        ], {
            duration: 2000,
            easing: "ease-in-out"
        }
    );
};
const ENTER = () => {
    document.querySelector(".pj").animate(
        [{
                backgroundPosition: 0
            },
            {
                backgroundPosition: 221 + "px"
            }
        ], {
            duration: 1500,
            easing: "steps(5)",
            iterations: Infinity
        }
    );
    gameLoopStart();
};

const isEnter = event => "Enter" === event.code;
const isSpace = event => "Space" === event.code;
const enterKeyDowns = keyDowns.filter(isEnter);
const spaceKeyDowns = keyDowns.filter(isSpace);

const enter = enterKeyDowns.scan(() => ENTER());
const space = spaceKeyDowns.scan(() => SPACE());

enter.subscribe(x => {
    x
});
space.subscribe(x => {
    x
});

//func para añadir ceros a la izquierda de un numero
const padLeft = (value, length) => {
    return (value.toString().length < length) ? padLeft("0" + value, length) : value;
};
//intervalo del movimiento del game
const gameLoopStart = () => {
    objectsArr.push(document.querySelector(".pj"));
    document.querySelector(".gameStart").remove();
    window.mainInterval = setInterval(() => {
        backgroundMovement();
        checkColisions();
        document.querySelector(".currentScore").textContent =
            padLeft(parseInt(document.querySelector(".currentScore").textContent, 10) + 1, 10);

    }, GAME_TIMER);

    window.cactusInterval = setInterval(() => {
        generateCactus();
    }, OBJECTS_TIMER);
};

const backgroundMovement = () => {
    if (fondos[0].style.right === "" &&
        fondos[1].style.left === "" ||
        fondos[0].style.right === "100%") {

        fondos[0].style.right = "0";
        fondos[1].style.left = "100%"
    }
    fondos[0].style.right = (parseInt(fondos[0].style.right) + VELOCIDAD_FONDO) + "%";
    fondos[1].style.left = (parseInt(fondos[1].style.left) - VELOCIDAD_FONDO) + "%";
}


//generacion de obstaculos en el fondo
const generateCactus = () => {
    const container = document.querySelector(".main-container");
    const cactus = document.createElement("div");
    cactus.style.background = "url('assets/obstacle-small.png')"
    cactus.classList.add("cactus");
    cactus.style.left = Math.round(Math.random() * (151 - 100) + 100) + "%";
    let animation = cactus.animate([{
            left: cactus.style.left
        }, {
            left: "-" + 20 + "px"
        }

    ], {
        duration: 5000
    });

    animation.onfinish = function () {
        objectsArr.splice(objectsArr.indexOf('foo'), 1);
        cactus.remove();
    };

    objectsArr.push(cactus);
    container.appendChild(cactus);
};

//checkea si el t-rex se ha chocado contra algun obstaculo
const checkColisions = () => {
    // if (colision) {
    //     clearInterval(window.mainInterval);
    //     clearInterval(window.cactusInterval);
    //     document.querySelectorAll(".cactus").forEach((cactus) => {
    //         cactus.remove()
    //     });

    //     document.querySelector(".pj").remove();
    //     const endGame = document.createElement("p");
    //     endGame.innerText = "Game Over";
    //     endGame.classList.add("gameOver");
    //     document.querySelector(".main-container").append(endGame);

    //     storeScore();
    // }

};

//guardar el score al terminar la partida
const storeScore = () => {
    const score = parseInt(document.querySelector(".currentScore").innerText);
    if (score > localStorage.getItem("maxPuntuation") ||
        localStorage.getItem("maxPuntuation") === null) {

        localStorage.setItem("maxPuntuation", parseInt(score));
    }
};